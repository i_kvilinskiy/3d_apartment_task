<?php
return [
    'pdo' => [
        'dsn' => 'mysql:host=127.0.0.1;dbname=notes',
        'user' => 'root',
        'password' => ''
    ],
    'uploads_folder' => 'public/uploads'
];
