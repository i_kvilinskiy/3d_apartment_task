<?php

require_once "vendor/autoload.php";

try {
    \App\Services\ServiceContainer::getInstance()
        ->getRouter()
        ->run();
} catch (Exception $e) {
    http_response_code($e->getCode());
    echo $e->getMessage();
}
