<?php

namespace App\Services;

use App\Services\DataBase\DataBaseInterface;
use App\Services\DataBase\MysqlDataBase;
use App\Services\FileUploader\FileUploader;
use App\Services\FileUploader\FileUploaderInterface;
use App\Services\Renderer\HTMLRenderer;
use App\Services\Renderer\RendererInterface;
use App\Services\Router\HttpRouter;
use App\Services\Router\HttpRouterKit;
use App\Services\Router\RouterInterface;
use App\Services\UrlBuilder\UrlBuilderInterface;

/**
 * Class ServiceContainer
 * @package App\Services
 */
class ServiceContainer
{
    /**
     * @var ServiceContainer|null
     */
    private static ?self $instance = null;

    /**
     * @var DataBaseInterface|MysqlDataBase
     */
    private DataBaseInterface $dataBase;
    /**
     * @var HTMLRenderer|RendererInterface
     */
    private RendererInterface $renderer;
    /**
     * @var HttpRouter|RouterInterface
     */
    private RouterInterface $router;
    /**
     * @var FileUploader|FileUploaderInterface
     */
    private FileUploaderInterface $fileUploader;
    /**
     * @var UrlBuilderInterface
     */
    private UrlBuilderInterface $urlBuilder;

    /**
     * @var array|null
     */
    private ?array $config = null;

    /**
     * ServiceContainer constructor.
     */
    private function __construct()
    {
        $this->renderer = new HTMLRenderer();

        $this->router = new HttpRouter(
            HttpRouterKit::getInstance()->getStrategy()
        );

        $this->urlBuilder = HttpRouterKit::getInstance()
            ->getUrlBuilder();

        $this->dataBase = new MysqlDataBase(
            $this->getConfig()['pdo']['dsn'],
            $this->getConfig()['pdo']['user'],
            $this->getConfig()['pdo']['password']
        );

        $this->fileUploader = new FileUploader();
    }

    /**
     * @return ServiceContainer|null
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }
    /**
     * @return DataBaseInterface
     */
    public function getDataBase(): DataBaseInterface
    {
        return $this->dataBase;
    }

    /**
     * @return RendererInterface
     */
    public function getRenderer(): RendererInterface
    {
        return $this->renderer;
    }

    /**
     * @return RouterInterface
     */
    public function getRouter(): RouterInterface
    {
        return $this->router;
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        if ($this->config === null) {
            $this->config = require_once realpath(__DIR__ . '/../../config.php');

            $localConfigPath = realpath(__DIR__ . '/../../config.local.php');

            if (file_exists($localConfigPath)) {
                $localConfig = require_once $localConfigPath;

                $this->config = array_merge($this->config, $localConfig);
            }
        }

        return $this->config;
    }

    /**
     * @return FileUploaderInterface
     */
    public function getFileUploader(): FileUploaderInterface
    {
        return $this->fileUploader;
    }

    /**
     * @return UrlBuilderInterface
     */
    public function getUrlBuilder(): UrlBuilderInterface
    {
        return $this->urlBuilder;
    }
}
