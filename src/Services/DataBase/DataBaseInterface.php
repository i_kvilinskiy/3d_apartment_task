<?php


namespace App\Services\DataBase;


use App\Models\DBModelInterface;

/**
 * Interface DataBaseInterface
 * @package App\Services\DataBase
 */
interface DataBaseInterface
{
    /**
     * @param DBModelInterface $model
     * @return bool
     */
    public function saveModel(DBModelInterface $model): bool;

    /**
     * @param string $modelClass
     * @param int $id
     * @return DBModelInterface|null
     */
    public function findModel(string $modelClass, int $id): ?DBModelInterface;

    /**
     * @param DBModelInterface $model
     * @return bool
     */
    public function removeModel(DBModelInterface $model): bool;

    /**
     * @param string $modelClass
     * @param int|null $limit
     * @param int|null $offset
     * @return DBModelInterface[]
     */
    public function findAllModels(string $modelClass, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param string $repositoryName
     * @return int|null
     */
    public function count(string $repositoryName): ?int;
}
