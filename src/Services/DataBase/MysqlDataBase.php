<?php


namespace App\Services\DataBase;


use App\Models\DBModelInterface;

/**
 * Class MysqlDataBase
 * @package App\Services\DataBase
 */
class MysqlDataBase implements DataBaseInterface
{
    /**
     * @var \PDO
     */
    private \PDO $pdo;

    /**
     * MysqlDataBase constructor.
     * @param string $dsn
     * @param string $user
     * @param string $password
     */
    public function __construct(string $dsn, string $user, string $password)
    {
        $this->pdo = new \PDO($dsn, $user, $password);
    }

    /**
     * @param DBModelInterface $model
     * @return bool
     */
    public function saveModel(DBModelInterface $model): bool
    {
        $data = $model->convertToArray();

        if (!empty($data)) {
            if ($model->getId() !== null) {
                $data['id'] = $model->getId();
            }

            $dataKeys = array_keys($data);

            $onDuplicateData = [];
            foreach ($dataKeys as $item) {
                $onDuplicateData[] = "`{$item}` = VALUES(`{$item}`)";
            }

            $sql = "
                INSERT INTO `{$model::getRepositoryName()}`
                    (`" . implode('`, `', $dataKeys) . "`)
                    VALUES (:" . implode(', :', $dataKeys) . ")
                ON DUPLICATE KEY UPDATE
                    " . implode(', ', $onDuplicateData) . "
            ;";

            $sth = $this->pdo->prepare($sql);

            if ($sth->execute($data)) {
                if (!$model->getId())
                    $model->setId($this->pdo->lastInsertId());

                return true;
            }
        }

        return false;
    }

    /**
     * @param string $modelClass
     * @param int $id
     * @return DBModelInterface|null
     */
    public function findModel(string $modelClass, int $id): ?DBModelInterface
    {
        $sql = "
            SELECT * 
            FROM {$modelClass::getRepositoryName()}
            WHERE `id` = :id 
        ";

        $sth = $this->pdo->prepare($sql);

        if (
            !$sth->execute(['id' => $id])
            || !($data = $sth->fetch(\PDO::FETCH_ASSOC))
        ) {
            return null;
        }

        return new $modelClass($data);
    }

    /**
     * @param DBModelInterface $model
     * @return bool
     */
    public function removeModel(DBModelInterface $model): bool
    {
        if (!$model->getId()) return false;

        $sql = "
            DELETE FROM {$model::getRepositoryName()}
            WHERE id = :id
        ";

        $sth = $this->pdo->prepare($sql);

        return $sth->execute(['id' => $model->getId()]);
    }

    /**
     * @param string $modelClass
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function findAllModels(string $modelClass, ?int $limit = null, ?int $offset = null): array
    {
        $data = [];

        $sql = "SELECT * FROM {$modelClass::getRepositoryName()}  ORDER BY `id` DESC";

        if ($limit) $sql .= " LIMIT {$limit}";
        if ($offset) $sql .= " OFFSET {$offset}";

        $sth = $this->pdo->prepare($sql);
        $sth->execute();
        $dbData = $sth->fetchAll(\PDO::FETCH_ASSOC);

        foreach ($dbData as $item) {
            $data[] = new $modelClass($item);
        }

        return $data;
    }

    /**
     * @param string $repositoryName
     * @return int|null
     */
    public function count(string $repositoryName): ?int
    {
        $cnt = $this->pdo
            ->query("SELECT COUNT(*) FROM {$repositoryName}")
            ->fetchColumn();

        return $cnt !== false ? (int)$cnt : null;
    }
}
