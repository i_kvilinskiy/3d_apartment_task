<?php


namespace App\Services\FileUploader;


/**
 * Interface FileUploaderInterface
 * @package App\Services\FileUploader
 */
interface FileUploaderInterface
{
    /**
     * Upload file. Return array with uploaded file name and path
     *
     * @param array $file
     * @return array|null
     */
    public function uploadFile(array $file): ?array;
}
