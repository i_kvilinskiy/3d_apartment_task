<?php


namespace App\Services\FileUploader;


use App\Services\ServiceContainer;

/**
 * Class FileUploader
 * @package App\Services\FileUploader
 */
class FileUploader implements FileUploaderInterface
{
    /**
     * @param array $file
     * @return array|null
     */
    public function uploadFile(array $file): ?array
    {
        $uploadsDir = ServiceContainer::getInstance()->getConfig()['uploads_folder'];

        $filePath = realpath(__DIR__ . '/../../../' . $uploadsDir);
        $fileExtension = substr($file['name'], strripos($file['name'], '.') + 1);
        $fileName = $this->generateRandomName($fileExtension);

        for ($i = 0; file_exists($filePath . '/' . $fileName); $i++) {
            $fileName = $this->generateRandomName($fileExtension);
        }

        if (!move_uploaded_file($file['tmp_name'], $filePath . '/' . $fileName)) {
            return null;
        }

        return [
            'file_name' => $fileName,
            'file_uri' => '/' . $uploadsDir . '/' . $fileName,
        ];
    }

    /**
     * @param $extension
     * @return string
     */
    private function generateRandomName($extension): string
    {
        return md5(time() . rand(16, 16)) . '.' . $extension;
    }
}
