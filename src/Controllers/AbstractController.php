<?php


namespace App\Controllers;


use App\Services\Renderer\RendererInterface;

/**
 * Class AbstractController
 * @package App\Controllers
 */
abstract class AbstractController implements ControllerInterface
{
    /**
     * @var RendererInterface
     */
    protected RendererInterface $renderer;

    /**
     * AbstractController constructor.
     * @param RendererInterface $renderer
     */
    public function __construct(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @param string $viewPath
     * @param array $params
     */
    public function render(string $viewPath, $params = []): void
    {
        $params = [
            '_viewPath' => $viewPath,
            '_vars' => $params
        ];

        $this->renderer->render('layout.php', $params);
    }
}
