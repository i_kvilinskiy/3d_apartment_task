<?php


namespace App\Controllers;


use App\Models\Image;
use App\Models\Note;
use App\Services\ServiceContainer;

/**
 * Class DefaultController
 * @package App\Controllers
 */
class DefaultController extends AbstractController
{
    public function actionIndex()
    {
        $paginationStep = 2;

        $page = $_GET['page'] ?? 1;
        $offset = $page > 1 ? $paginationStep * ($page - 1) : 0;
        $pagesNumber = ceil(
            ServiceContainer::getInstance()->getDataBase()->count(Note::getRepositoryName())
            / $paginationStep
        );

        $models = ServiceContainer::getInstance()
            ->getDataBase()
            ->findAllModels(Note::class, $paginationStep, $offset);

        $this->render('default/index.php', [
            'models' => $models,
            'pagesNumber' => $pagesNumber,
            'page' => $page
        ]);
    }

    public function actionCreate()
    {
        $model = new Note();
        $this->renderNoteForm($model, 'Добавить');
    }

    public function actionDelete()
    {
        if (isset($_GET['id'])) {
            $model = ServiceContainer::getInstance()
                ->getDataBase()
                ->findModel(Note::class, $_GET['id']);

            if ($model) {
                ServiceContainer::getInstance()
                    ->getDataBase()
                    ->removeModel($model);
            }
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        return;
    }

    public function actionUpdate()
    {
        $id = $_GET['id'] ?? null;

        if (
            $id == null
            || ($model = ServiceContainer::getInstance()->getDataBase()->findModel(Note::class, $id)) == null
        ) {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
            return;
        }

        /**
         * @var Note $model
         */
        $this->renderNoteForm($model, 'Обновить', false);
    }

    /**
     * @param Note $model
     * @param $title
     * @param bool $flushAfterSave
     */
    private function renderNoteForm(Note $model, $title, $flushAfterSave = true)
    {
        $alert = [];
        if (!empty($_POST)) {
            $model->load($_POST);

            if (!$this->validateNoteModel($model)) {
                $alert = [
                    'type' => 'danger',
                    'message' => 'Необходимо заполнить все обязательные поля!',
                ];
            } else {
                if ($this->saveNoteModelFromForm($model)) {
                    $alert = [
                        'type' => 'success',
                        'message' => 'Данные успешно сохранены!',
                    ];

                    if ($flushAfterSave) $model = new Note();
                } else {
                    $alert = [
                        'type' => 'danger',
                        'message' => 'Серверная ошибка',
                    ];
                }
            }
        }

        $this->render('default/form.php', [
            '_title' => $title,
            'model' => $model,
            'alert' => $alert,
        ]);
    }

    /**
     * @param Note $model
     * @return bool
     */
    private function validateNoteModel(Note $model): bool
    {
        return $model->getTitle() !== null && $model->getContent() !== null;
    }

    /**
     * @param Note $model
     * @return bool
     */
    private function saveNoteModelFromForm(Note $model): bool
    {
        if (isset($_FILES['imageFile']) && !empty(isset($_FILES['imageFile']))) {
            $imageModel = new Image();

            $type = explode('/', $_FILES['imageFile']['type']);

            if ($type[0] == 'image') {

                $file = ServiceContainer::getInstance()
                    ->getFileUploader()
                    ->uploadFile($_FILES['imageFile']);

                if ($file) {
                    $imageModel
                        ->setUri($file['file_uri'])
                        ->setFileName($file['file_name']);

                    ServiceContainer::getInstance()
                        ->getDataBase()
                        ->saveModel($imageModel);

                    $model->setImageId($imageModel->getId());
                }
            }
        }

        return ServiceContainer::getInstance()
            ->getDataBase()
            ->saveModel($model);
    }
}
