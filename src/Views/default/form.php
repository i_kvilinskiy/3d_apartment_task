<?php
/**
 * @var \App\Models\Note $model
 * @var array $alert
 * @var \App\Services\Renderer\RendererInterface $renderer
 */

$isNewModel = $model->getId() === null;
?>

<?php if (!empty($alert)): ?>
    <?php $renderer->render('_components/alert.php', $alert) ?>
<?php endif; ?>

<div class="card">
    <?php if (($image = $model->getImage()) !== null): ?>
        <img class="card-img-top"
             src="<?= $image->getUri() ?>"
             alt="<?= $image->getFileName() ?>">
    <?php endif; ?>

    <div class="card-body">
        <form method="post" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-group">
                    <label for="title">Загловок</label>
                    <input type="text" class="form-control" id="title" value="<?= $model->getTitle() ?>" name="title"
                           placeholder="Заметка 1...">
                </div>
                <div class="form-group">
                    <label for="content">Содержимое</label>
                    <textarea class="form-control" id="content" name="content"
                              placeholder="Сделать..."><?= $model->getContent() ?></textarea>
                </div>
                <div class="form-group">
                    <label for="imageFile"><?= $isNewModel ? 'Изображение' : 'Новое изображение' ?></label>
                    <input type="file" class="form-control-file" id="imageFile" name="imageFile">
                </div>
            </div>
            <button class="btn btn-success btn-block"><?= $isNewModel ? 'Сохранить' : 'Обновить' ?></button>
        </form>
    </div>
</div>
