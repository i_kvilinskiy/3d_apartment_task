<?php
/**
 * @var \App\Models\Note[] $models
 * @var int $pagesNumber
 * @var int $page
 */
?>

<?php foreach ($models as $model): ?>
    <div class="card mb-4">
        <?php if (($image = $model->getImage()) !== null): ?>
            <img class="card-img-top"
                 src="<?= $image->getUri() ?>"
                 alt="<?= $image->getFileName() ?>">
        <?php endif; ?>


        <div class="card-body">
            <h5 class="card-title"><?= $model->getTitle() ?> (<?= $model->getId() ?>)</h5>
            <p class="card-text"><?= $model->getContent() ?></p>

            <?php
                $deleteLink = \App\Services\ServiceContainer::getInstance()->getUrlBuilder()
                    ->setController('default')
                    ->setAction('delete')
                    ->setParams(['id' => $model->getId()])
                    ->getUrl();

                $updateLink = \App\Services\ServiceContainer::getInstance()->getUrlBuilder()
                    ->setController('default')
                    ->setAction('update')
                    ->setParams(['id' => $model->getId()])
                    ->getUrl();
            ?>
            <a href="<?= $updateLink ?>" class="btn btn-outline-primary btn-sm">Редактировать</a>
            <a href="<?= $deleteLink ?>"
               class="btn btn-outline-danger btn-sm"
               onclick="return confirm('Удалить запись?')">Удалить</a>
        </div>
    </div>
<?php endforeach; ?>

<nav aria-label="Page navigation example">
    <ul class="pagination">
        <?php for ($i = 1; $i <= $pagesNumber; $i++): ?>
            <li class="page-item<?= $i == $page ? ' active' : '' ?>">
                <?php $pageLink = \App\Services\ServiceContainer::getInstance()->getUrlBuilder()
                    ->setController('default')
                    ->setAction('index')
                    ->setParams(['page' => $i])
                    ->getUrl()
                ?>

                <a class="page-link" href="<?= $pageLink ?>"><?= $i ?></a>
            </li>
        <?php endfor; ?>
    </ul>
</nav>


