<?php


namespace App\Models;


/**
 * Interface DBModelInterface
 * @package App\Models
 */
interface DBModelInterface extends ModelInterface
{
    /**
     * @return string
     */
    public static function getRepositoryName(): string;

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @param int $id
     * @return DBModelInterface
     */
    public function setId(int $id): self;
}
