<?php


namespace App\Models;


/**
 * Interface ModelInterface
 * @package App\Models
 */
interface ModelInterface
{
    /**
     * ModelInterface constructor.
     * @param array $params
     */
    public function __construct(array $params = []);

    /**
     * @param array $data
     * @return mixed
     */
    public function load(array $data);

    /**
     * @return array
     */
    public function convertToArray(): array;
}
