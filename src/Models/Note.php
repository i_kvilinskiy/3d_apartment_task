<?php


namespace App\Models;


use App\Services\ServiceContainer;

/**
 * Class Note
 * @package App\Models
 */
class Note extends AbstractDBModel
{
    /**
     * @var string
     */
    private ?string $title = null;

    /**
     * @var string
     */
    private ?string $content = null;

    /**
     * @var int
     */
    private ?int $image_id = null;

    /**
     * @var Image
     */
    private ?Image $_image = null;

    /**
     * @return string
     */
    public static function getRepositoryName(): string
    {
        return 'notes';
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Note
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getImageId(): ?int
    {
        return $this->image_id;
    }


    /**
     * @param int $imageId
     * @return Note
     */
    public function setImageId(int $imageId): self
    {
        $this->image_id = $imageId;

        return $this;
    }

    /**
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        if ($this->_image == null && $this->getImageId() != null) {
            $this->_image = ServiceContainer::getInstance()
                ->getDataBase()
                ->findModel(Image::class, $this->getImageId());
        }

        return $this->_image;
    }
}
