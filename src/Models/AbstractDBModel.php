<?php


namespace App\Models;


/**
 * Class AbstractDBModel
 * @package App\Models
 */
abstract class AbstractDBModel extends AbstractModel implements DBModelInterface
{
    /**
     * @var int|null
     */
    protected ?int $id = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DBModelInterface
     */
    public function setId(int $id): DBModelInterface
    {
        $this->id = $id;

        return $this;
    }
}
