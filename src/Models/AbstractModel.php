<?php


namespace App\Models;

use ReflectionClass;
use ReflectionProperty;

/**
 * Class AbstractModel
 * @package App\Models
 */
abstract class AbstractModel implements ModelInterface
{
    /**
     * AbstractModel constructor.
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        if (!empty($params)) $this->load($params);
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function convertToArray(): array
    {
        $data = [];

        $reflection = new ReflectionClass($this);
        $vars = $reflection->getProperties(ReflectionProperty::IS_PRIVATE);

        foreach ($vars as $var) {
            if (substr($var->getName(), 0, 1) != '_') {
                $methodName = 'get' . $this->getMethodNameByColumn($var->getName());

                if (method_exists($this, $methodName)) {
                    $data[$var->getName()] = $this->$methodName();
                }
            }
        }

        return $data;
    }

    /**
     * @param array $data
     */
    public function load(array $data)
    {
        foreach ($data as $key => $value) {
            $methodName = 'set' . $this->getMethodNameByColumn($key);

            if (method_exists($this, $methodName) && $value !== null)
                $this->$methodName($value);
        }
    }

    /**
     * @param string $column
     * @return string
     */
    private function getMethodNameByColumn(string $column)
    {
        $exploded = array_map(function ($item) {
            return ucfirst($item);
        }, explode('_', $column));

        return implode('', $exploded);
    }
}
