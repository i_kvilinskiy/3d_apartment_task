<?php


namespace App\Models;


/**
 * Class Image
 * @package App\Models
 */
class Image extends AbstractDBModel implements DBModelInterface
{
    /**
     * @var string|null
     */
    private ?string $file_name = null;

    /**
     * @var string|null
     */
    private ?string $uri = null;

    /**
     * @return string
     */
    public static function getRepositoryName(): string
    {
        return 'images';
    }

    /**
     * @return string
     */
    public function getFileName(): ?string
    {
        return $this->file_name;
    }

    /**
     * @param string $fileName
     * @return Image
     */
    public function setFileName(string $fileName): self
    {
        $this->file_name = $fileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): ?string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return Image
     */
    public function setUri(string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }
}
